@echo off

:: Adding and deleting users from OUs and stuff 
:: By kudaranai 
:: This was a homework script, it's not meant to be used irl

:: Create OU with my name
dsadd ou "ou=Me, dc=ActiveDirectory, dc=local"

:: Create OUs
for /L %%a in (1,1,4) do (
    dsadd ou "ou=%%agroup, ou=Me, dc=ActiveDirectory, dc=local"
    for /L %%b in (1,1,6) do (
        dsadd ou "ou=%%agroupnumber%%b, ou=%%agroup, ou=Me, dc=ActiveDirectory, dc=local"
        for /L %%c in (1,1,30) do (
            dsadd user "cn=User-%%a-%%b-%%c, ou=%%agroupnumber%%b, ou=%%agroup, ou=Me, dc=ActiveDirectory, dc=local"
        )
    )
)