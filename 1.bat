@echo off

:: Adding and deleting users from OUs and stuff
:: By kudaranai
:: This was a homework script, it's not meant to be used irl

:: Create users in the Users CN

for /L %%a in (1,1,30) do (
	dsadd user "cn=User-%%a, cn=Users, dc=ActiveDirectory, dc=local" -pwd User_%%a
    )

    for /L %%a in (1,1,10) do (
    	dsadd user "cn=Teacher-%%a, cn=Users, dc=ActiveDirectory, dc=local" -pwd Teacher_%%a
        )

:: Delete users from the users CN

    for /L %%a in (1,1,30) do (
        dsrm "cn=User-%%a, cn=Users, dc=ActiveDirectory, dc=local" -noprompt
        )

        for /L %%a in (1,1,10) do (
            dsrm "cn=Profesor-%%a, cn=Users, dc=ActiveDirectory, dc=local" -noprompt
            )

:: Create OUs for each type of user

dsadd ou "ou=UsersOU, dc=ActiveDirectory, dc=local"
dsadd ou "ou=TeachersOU, dc=ActiveDirectory, dc=local"

:: Create users in their OUs

for /L %%a in (1,1,30) do (
    dsadd user "cn=User-%%a, ou=UsersOU, dc=ActiveDirectory, dc=local" -pwd User_%%a
)

for /L %%a in (1,1,10) do (
    dsadd user "cn=Teacher-%%a, ou=TeachersOU, dc=ActiveDirectory, dc=local" -pwd Teacher_%%a
)
